# Custom Questionnaire

A complete questionnaire solution.

## Installation

Put the c_questionnaire folder in your custom modules directory and enable it.

A new content type "questonnaire" will be generated.

## Details

### Questionnaire node form

Inside the questionnaire node form there are the following inputs:

* Questionnaire title  - The questionnaire informative title.
* Questions - The questions of the questionnaire with :
   * Question title - The question.
   * Question's possible answers - The possible answers of the question with:
      * Possible answer's title - One possible answer.
      * Is the right answer - A checkbox to check for the right anser.
   * Question's difficulty - The difficulty of the question from 1 to 5, it also affects the amount of points the user will get.
* Questionnaire supervisors - A list of site authenticated users that can have access to the questionnaire results.

### Questionnaire form

After completing the previous steps and save the node, when we go to node view page we are going to see a questionnaire form,
that only authenticated users can take once.

Anonymous users can see the questionnaire but instead of a submit button they have a link to the register page.

In the node view page, there is also a "Results" tab with all the users' results that have taken this specific questionnaire,
only admin, author and questionnaire supervisors can have access to the tab.

There is another tab in every authenticated user profile page with the title "Questionnaire results" that shows all the results of questionnaires this user has taken and is accessible only by the user.
